import java.util.Scanner;
public class SumOfTwoMatrices {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		int size1, size2, Size1, Size2, i, j;		
		System.out.println("Matrix A");
		do
		{
			System.out.print("Input the number of line size1: ");
			size1 = keyboard.nextInt();
			System.out.print("Input the number of column size2: ");
			size2 = keyboard.nextInt();
			if (size1 <= 0 || size2 <= 0)
				{
				System.out.println("size1, size2 must be an postive integer");
				continue;
				}
		} while (0==1);
		int A[][] = new int[size1][size2];
		for (i = 0; i < size1; i++) {
			for (j = 0; j < size2; j++) {
				System.out.print("A[" + (i+1) + "][" + (j+1) + "] = ");
				A[i][j] = keyboard.nextInt();
			}
		}
		System.out.print("\n");		
		System.out.println("Matrix B");
		do
		{
			System.out.print("Input the number of line Size1: ");
			Size1 = keyboard.nextInt();
			System.out.print("Input the number of column Size2: ");
			Size2 = keyboard.nextInt();
			if (Size1 <= 0 || Size2 <= 0)
				{
				System.out.println("Size1, Size2 must be an postive integer");
				continue;
				}
		} while (0 == 1);
		int B[][] = new int[Size1][Size2];
		for (i = 0; i < Size1; i++) {
			for (j = 0; j < Size2; j++) {
				System.out.print("B[" + (i+1) + "][" + (j+1) + "] = ");
				B[i][j] = keyboard.nextInt();
			}
		}
		System.out.println(" ");		
		if (size1 != Size1 || size2 != Size2) System.out.println("Two matrix don't have same size. Cannot compute sum of them.");
		else {
			System.out.println("The sum matrix of matrix A and matrix B is: ");
			for (i = 0; i < size1; i++) {
				for (j = 0; j < size2; j++) {
					System.out.print((A[i][j] + B[i][j]) + "\t");
				}
				System.out.println();
			}
		}		
		System.exit(0);
	}
}