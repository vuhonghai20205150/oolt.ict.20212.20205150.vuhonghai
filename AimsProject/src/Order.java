
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	int i;
	float sum = 0;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	int qtyOrdered = 0;
	public void addDigitalVideoDisc(DigitalVideoDisc dvd) {
		if(qtyOrdered <MAX_NUMBERS_ORDERED) {
			for(i=0;i<MAX_NUMBERS_ORDERED;i++)
			{
				if(itemsOrdered[i]== null)
				{
					itemsOrdered[i] = dvd;
					break;
				}
			}
			qtyOrdered++;
		}
		else
		{
			System.out.println("The array is full.");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc dvd) {
		if(qtyOrdered >= 0) {
			for(i=0;i<MAX_NUMBERS_ORDERED;i++)
			{
				if(itemsOrdered[i] == dvd)
				itemsOrdered[i]= null;
			}
			qtyOrdered--;
		}
		else
		{
			System.out.println("The array is empty.");
		}
	}
	public float totalCost()
	{
		sum = 0;
		for(i=0;i<MAX_NUMBERS_ORDERED;i++)
		{
			if(itemsOrdered[i] == null)
				continue;
			sum = sum + itemsOrdered[i].getCost();
		}
		return sum;
	}
}
