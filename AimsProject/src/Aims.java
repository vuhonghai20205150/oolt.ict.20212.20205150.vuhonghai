
public class Aims {

	public static void main(String[] args) {
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc();
		dvd1.setTitle("The Lion King");
		dvd1.setCategory ("Animation");
		dvd1.setCost (19.95f);
		dvd1.setDirector ("Roger Allers");
		dvd1.setLength (87);
		// add the dvd to the order
		anOrder.addDigitalVideoDisc(dvd1);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc();
		dvd2.setTitle("Star Wars");
		dvd2.setCategory ("Science Fiction");
		dvd2.setCost (24.95f);
		dvd2.setDirector ("George Lucas");
		dvd2.setLength (124);
		anOrder.addDigitalVideoDisc(dvd2);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc();
		dvd3.setTitle("Aladdin");
		dvd3. setCategory ("Animation");
		dvd3. setCost (18.99f);
		dvd3.setDirector ("John Musker");
		dvd3. setLength (90);
		// add the dyd to the order
		anOrder.addDigitalVideoDisc(dvd3);
		System.out.print ("Total Cost is: ");
		System.out.println(anOrder.totalCost());
		anOrder.removeDigitalVideoDisc(dvd3);
		System.out.println("Total Cost after remove disk 3 is: " + anOrder.totalCost());
		
	}

}
