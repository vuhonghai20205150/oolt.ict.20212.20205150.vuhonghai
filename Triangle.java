import java.util.Scanner;

public class Triangle {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int j, n, i,a;
		System.out.print("Enter n: ");
		n = keyboard.nextInt();
		for(a=1;a<=n;a++)
		{
			for(j=(n-a);j>0;j--)
			{
				System.out.print(" ");
			}
			for(i=1; i<= 2*(a-1)+1; i++)
			{
				System.out.print("*");
			}
			System.out.print("\n");
		}
	}

}
