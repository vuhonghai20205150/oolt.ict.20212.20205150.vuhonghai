import java.util.Scanner;
import java.lang.Math;
public class EquationSolve {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int choice;
		double A, B, C, A1, A2, B1, B2, C1, C2, D, D2, D1, X, X1, X2, delta;
		
		boolean e = 0 == 0;
		while(e)
		{
			System.out.println("======Solve========\n"
					+ "1.The first-degree equation (linear equation) with one variable\nA X + B = C (A != 0)"
					+ "\n2.The system of first-degree equations (linear system) with two variables\n"
					+ "|A1 X1 + B1 X2 = C1\n"
					+ "|A2 X1 + B2 X2 = C2\n"
					+ "3.The second-degree equation with one variable\n"
					+ "A X^2 + B X + C = 0\n"
					+ "4.Exit");
			System.out.print("Enter your choice: ");
			choice = input.nextInt();
			switch(choice)
			{
			case 1:
			{
				System.out.println("Solve: A X + B = C (A != 0)");
				do {
					System.out.print("Enter A: ");
					A = input.nextDouble();
				}while(A==0);
				System.out.print("Enter B: ");
				B = input.nextDouble();
				System.out.print("Enter C: ");
				C = input.nextDouble();
				X = (C-B)/A;
				System.out.println("Result: X = " + X);
			}
			break;
			case 2:
			{
				System.out.println("Solve: |A1 X1 + B1 X2 = C1\n"
						+ "       |A2 X1 + B2 X2 = C2");
				System.out.print("Enter A1: ");
				A1 = input.nextDouble();
				System.out.print("Enter B1: ");
				B1 = input.nextDouble();
				System.out.print("Enter C1: ");
				C1 = input.nextDouble();
				System.out.print("Enter A2: ");
				A2 = input.nextDouble();
				System.out.print("Enter B2: ");
				B2 = input.nextDouble();
				System.out.print("Enter C2: ");
				C2 = input.nextDouble();
				D = A1*B2 - A2*B1;
				D1 = C1*B2 - C2*B1;
				D2 = A1*C2 - A2*C1;
				if(D != 0)
				{
					X1 = D1/D;
					X2 = D2/D;
					System.out.println("X1 = " + X1 +"\nX2 = " + X2);
				}else if(D1 == D2 && D2 == D)
				{
					System.out.println("System has infinitely many solutions!");
					
				}else
				{
					System.out.println("System has no solution!");
				}
			}
			break;
			case 3:
			{
				System.out.println("Solve: A X^2 + B X + C = 0");
				do {
					System.out.print("Enter A: ");
					A = input.nextDouble();
				}while(A==0);
				System.out.print("Enter B: ");
				B = input.nextDouble();
				System.out.print("Enter C: ");
				C = input.nextDouble();
				delta = B*B - 4*A*C;
				if(delta == 0)
				{
					X1 = -B/(2*A);
					System.out.println("X1 = X2 ="+ X1);
				}else if(delta>0)
				{
					X1 = (-B + java.lang.Math.sqrt(delta))/(2*A);
					X2 = (-B - java.lang.Math.sqrt(delta))/(2*A);
					System.out.println("X1 = "+X1+"\nX2 = "+ X2); 
				}else
				{
					System.out.println("The equation has no solution!");
				}
			}
			break;
			case 4:
			{
				System.exit(0);
			}
			default:
			{
				System.out.println("You must enter a number from 1->4!");
			}
			}
		}
		System.exit(0);
	}

}
