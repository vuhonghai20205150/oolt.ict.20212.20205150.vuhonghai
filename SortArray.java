
public class SortArray {
	public static void main(String args[])
	{
		int[] array = {9,5,4,3,6,8};
		int i, j, temp, sum =0;
		System.out.println("Array before sorted: ");
		for(i=0;i<6;i++)
		{
			System.out.print(array[i]+"  ");
			sum += array[i];
		}
		for(i=0;i<6;i++)
		{
			for(j=i+1;j<6;j++)
			{
				if(array[i] > array[j])
				{
					temp = array[i];
					array[i]= array[j];
					array[j]= temp;
				}
			}
		}
		System.out.println("\nArray after sorted:");
		for(i=0;i<6;i++)
			System.out.print(array[i]+"  ");
		System.out.println("\nSum of array: " + sum );
		System.out.println("Average: "+(double)sum/6);
	}
}
